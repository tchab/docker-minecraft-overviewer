#!/usr/bin/env bash

set -x

MINECRAFT_WORLD_ROOT="/data/conf/nukkit/worlds"
OVERVIEWER_WORLD_ROOT="/data/conf/overviewer/worlds"
OVERVIEWER_SITE_DATA="/data/conf/overviewer/site"

if [[ -d "${OVERVIEWER_SITE_DATA}" ]]; then
    echo "Found existing overviewer data, renaming ..."
    timestamp="$(date '+%F_%H.%M.%S')"
    mv -v "${OVERVIEWER_SITE_DATA}" "${OVERVIEWER_SITE_DATA}.${timestamp}"
    mkdir -p "${OVERVIEWER_SITE_DATA}"
fi

echo "Updating overviewer worlds copy ..."
rsync -avu --stats --partial --progress --delete "${MINECRAFT_WORLD_ROOT}"/ "${OVERVIEWER_WORLD_ROOT}"/

docker run --rm \
    -v /data/conf/overviewer:/data overviewer

# Fix perms
chmod -R u+rwX,go+rX,go-w "${OVERVIEWER_SITE_DATA}"

