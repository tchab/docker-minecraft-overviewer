ARG RELEASE=18.04
ARG BASEIMAGE=docker.io/ubuntu:${RELEASE}

FROM ${BASEIMAGE} as builder

ARG DEBIAN_FRONTEND=noninteractive

ARG USER_ID=1000
ARG USERNAME=abc
ARG APP_DIR=/app

ENV GIT_URL="git://github.com/overviewer/Minecraft-Overviewer.git"
ENV MINECRAFT_VERSION="1.15"

RUN apt update \
    && apt -y dist-upgrade \
    && apt -y install --no-install-recommends \
             git wget curl build-essential \
             python3 python3-dev python3-distutils python3-pip python3-pil devscripts \
    && rm -rf /var/lib/apt/lists/* \
    && useradd -u "${USER_ID}" -m "${USERNAME}" \
    && mkdir -p "${APP_DIR}" \
    && chown -R "${USER_ID}" "${APP_DIR}"

WORKDIR ${APP_DIR}

USER ${USERNAME}

RUN git clone "${GIT_URL}" . \
    && python3 -m pip install numpy \
    && python3 setup.py build \
    && mkdir -p ~/.minecraft/versions/"${MINECRAFT_VERSION}" \
    && wget -q https://overviewer.org/textures/"${MINECRAFT_VERSION}" -O ~/.minecraft/versions/${MINECRAFT_VERSION}/${MINECRAFT_VERSION}.jar

CMD [ "python3", "overviewer.py", "--config=/data/config.py" ]
