# Description

Simple docker wrapper to use [Minecraft Overviewer](https://overviewer.org/)

# Usage

- Clone the repo and build the image :

```bash
git clone https://gitlab.com/tchab/docker-minecraft-overviewer.git
cd docker-minecraft-overviewer
docker build -t overviewer .
```

- Create data volume :

```bash
mkdir -p ~/.overviewer/worlds
```

- Create a sample configuration file _~/.overviewer/config.py_ :

```python
outputdir = "/data/site"
worlds_root = '/data/worlds'

worlds["world"] = f'{worlds_root}/world'

renders["world_ne"] = {
    "world": "world",
    "title": "World - North East",
    "rendermode": "smooth_lighting",
    "northdirection": "upper-right",
}

renders["world_se"] = {
    "world": "world",
    "title": "World - South East",
    "rendermode": "smooth_lighting",
    "northdirection": "lower-right",
}

renders["world_nw"] = {
    "world": "world",
    "title": "World - North West",
    "rendermode": "smooth_lighting",
    "northdirection": "upper-left",
}

renders["world_sw"] = {
    "world": "world",
    "title": "World - South West",
    "rendermode": "smooth_lighting",
    "northdirection": "lower-left",
}
```

- Copy Minecraft world :

```bash
rsync -avu --stats --partial --progress --delete /path/to/your/minecraft/world/ ~/.overviewer/worlds/
```

- Run container with the data dir mounted as volume :

```bash
docker run -it --rm -v ~/.overviewer:/data overviewer
```

If everything goes OK, generated webapp should be available under _~/.overviewer/site_